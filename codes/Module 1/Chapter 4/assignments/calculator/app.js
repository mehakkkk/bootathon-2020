var num1 = document.getElementById("num1");
var num2 = document.getElementById("num2");
var result = document.getElementById("result");
//to calculate value depending on operator
function calculate(op) {
    check();
    switch (op) {
        case '+':
            var c = parseFloat(num1.value) + parseFloat(num2.value);
            break;
        case '-':
            var c = parseFloat(num1.value) - parseFloat(num2.value);
            break;
        case '*':
            var c = parseFloat(num1.value) * parseFloat(num2.value);
            break;
        case '/':
            var c = parseFloat(num1.value) / parseFloat(num2.value);
            break;
    }
    result.value = c.toString();
}
//check whether number is not a string
function check() {
    var t1 = parseFloat(num1.value);
    var t2 = parseFloat(num2.value);
    if (isNaN(t1) || isNaN(t2)) {
        alert("not a number");
    }
}
//clear the result space
//# sourceMappingURL=app.js.map