function operate(){
    //string taken in t1 variable
    var t1: HTMLInputElement=<HTMLInputElement>document.getElementById("t1");
    var t1_value = t1.value;
    var op: HTMLDivElement=<HTMLDivElement>document.getElementById("op");
    var p: HTMLParagraphElement=<HTMLParagraphElement>document.getElementById("display");
    p.innerHTML = '<b>String entered is:</b>'+t1_value.toString();
    //i1,i2 for substring index
    //p1 to display substring message for index
    var p1: HTMLParagraphElement=<HTMLParagraphElement>document.getElementById("substring");
    p1.innerHTML = '<b>Enter Index to get Substring from original string:</b>'
    var i1:HTMLInputElement=<HTMLInputElement>document.getElementById("i1");
    var i2: HTMLInputElement=<HTMLInputElement>document.getElementById("i2");
    var i1 = document.createElement("input");
    i1.setAttribute("id","index1");  //set id for i1 input
    i1.setAttribute("placeholder","enter start index");
    var i2 = document.createElement("input");
    i2.setAttribute("id","index2");  //set if for i2 input
    i2.setAttribute("placeholder","enter end index");
    op.appendChild(i1);
    op.appendChild(i2);
    //create button
    var btn1 = document.createElement("button");
    btn1.setAttribute("id","sub");
    btn1.innerText="substring";
    op.appendChild(btn1);

    
    //eventlistener for substring button to display substring
    btn1.addEventListener("click",function(){
        var sub = t1_value.substring(Number(i1.value),Number(i2.value));
        p1.innerHTML = '<b>substring is:</b>'+sub.toString();
    });
      

    //To find index of variable given by the user from the string
    var variable = document.createElement("input");
    variable.setAttribute("id","find");
    variable.setAttribute("placeholder","Enter character to find its index");
    var search: HTMLDivElement=<HTMLDivElement>document.getElementById("search");
    search.appendChild(variable);
    var btn2 = document.createElement("button");
    btn2.setAttribute("id","btn2");
    btn2.innerText="Get Index";
    search.appendChild(btn2);
    //eventlistener for getting index of the variable
    btn2.addEventListener("click",function(){
        var n = t1_value.indexOf(variable.value);
        
        var display_index: HTMLParagraphElement=<HTMLParagraphElement>document.getElementById("display_index");
        if(n != -1)//if index not present then n will -1 else index of variable
        display_index.innerHTML = '<b>Index of variable '+variable.value+' is:</b>' + n.toString();
        else
        display_index.innerHTML = '<b>Variable not present</b>';
    })
}
