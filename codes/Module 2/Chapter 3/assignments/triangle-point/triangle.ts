//area function
function area(x1,y1,x2,y2,x3,y3){
    var area:number = Math.abs((x1*(y2-y3)+x2*(y3-y1)+x3*(y1-y2))/2.0);
    return area;

}
function check(){
    var x1: HTMLInputElement=<HTMLInputElement>document.getElementById("x1");
    var y1: HTMLInputElement=<HTMLInputElement>document.getElementById("y1");
    var x2: HTMLInputElement=<HTMLInputElement>document.getElementById("x2");
    var y2: HTMLInputElement=<HTMLInputElement>document.getElementById("y2");
    var x3: HTMLInputElement=<HTMLInputElement>document.getElementById("x3");
    var y3: HTMLInputElement=<HTMLInputElement>document.getElementById("y3");
    var p1: HTMLInputElement=<HTMLInputElement>document.getElementById("p1");
    var p2: HTMLInputElement=<HTMLInputElement>document.getElementById("p2");

    var A = area(x1.value,y1.value,x2.value,y2.value,x3.value,y3.value) //area of triangle

    //area of triangle wrt point
    var a1 = area(p1.value,p2.value,x2.value,y2.value,x3.value,y3.value);
    var a2 = area(x1.value,y1.value,p1.value,p2.value,x3.value,y3.value);
    var a3 = area(x1.value,y1.value,x2.value,y2.value,p1.value,p2.value);
    var display: HTMLParagraphElement=<HTMLParagraphElement>document.getElementById("display");
    if(A == (a1+a2+a3))//if this condition get satisfied then point lies inside triangle
    display.innerHTML = "Point lies inside triangle";
    else
    display.innerHTML = "Point does not lies inside triangle";

}