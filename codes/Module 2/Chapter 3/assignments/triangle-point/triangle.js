function area(x1, y1, x2, y2, x3, y3) {
    var area = Math.abs((x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)) / 2.0);
    return area;
}
function check() {
    var x1 = document.getElementById("x1");
    var y1 = document.getElementById("y1");
    var x2 = document.getElementById("x2");
    var y2 = document.getElementById("y2");
    var x3 = document.getElementById("x3");
    var y3 = document.getElementById("y3");
    var p1 = document.getElementById("p1");
    var p2 = document.getElementById("p2");
    var A = area(x1.value, y1.value, x2.value, y2.value, x3.value, y3.value); //area of triangle
    //area of triangle wrt point
    var a1 = area(p1.value, p2.value, x2.value, y2.value, x3.value, y3.value);
    var a2 = area(x1.value, y1.value, p1.value, p2.value, x3.value, y3.value);
    var a3 = area(x1.value, y1.value, x2.value, y2.value, p1.value, p2.value);
    var display = document.getElementById("display");
    if (A == (a1 + a2 + a3))
        display.innerHTML = "Point lies inside triangle";
    else
        display.innerHTML = "Point does not lies inside triangle";
}
//# sourceMappingURL=triangle.js.map