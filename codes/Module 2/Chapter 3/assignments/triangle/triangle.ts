function check(){
    var a: HTMLInputElement=<HTMLInputElement>document.getElementById("side1");
    var a_value = Number(a.value);
    var b: HTMLInputElement=<HTMLInputElement>document.getElementById("side2");
    var b_value = Number(b.value);
    var c: HTMLInputElement=<HTMLInputElement>document.getElementById("side3");
    var c_value = Number(c.value);
    var display: HTMLParagraphElement=<HTMLParagraphElement>document.getElementById("display");
    if(a_value!=b_value && b_value!=c_value && c_value!=a_value)
    {
        //if it is scalene triangle then execute these statements
        display.innerHTML = '<h3>Type of triangle is:<b><u>Scalene Triangle</b></u></h3>';
    }
    else if(a_value==b_value && b_value==c_value && a_value==c_value)
    {
        //if it is equilateral triangle then execute these statements
        display.innerHTML = '<h3>Type of triangle is:<b><u>Equilateral Triangle</b></u></h3>';
    }
    else
    {
        //if it is isosceles triangle then execute these statements
        display.innerHTML = '<h3>Type of triangle is:<b><u>Isosceles Triangle</b></u></h3>';
    }

    //now to check whether the triangle is right angle or not
    if(a_value>b_value && a_value>c_value)
    {
        if((a_value*a_value) == (b_value*b_value)+(c_value*c_value))
        display.innerHTML+= '<h3>It is a right-angled triangle</h3>';
        else 
        display.innerHTML+= '<h3>It is not a right-angled triangle</h3>';
    }
    if(b_value>c_value && b_value>a_value)
    {
        if((b_value*b_value) == (c_value*c_value)+(a_value*a_value))
        display.innerHTML+= '<h3>It is a right-angled triangle</h3>';
        else 
        display.innerHTML+= '<h3>It is not a right-angled triangle</h3>';
    }
    if(c_value>a_value && c_value>b_value)
    {
        if((c_value*c_value) == (a_value*a_value)+(b_value*b_value))
        display.innerHTML+= '<h3>It is a right-angled triangle</h3>';
        else 
        display.innerHTML+= '<h3>It is not a right-angled triangle</h3>';
    }
   

}