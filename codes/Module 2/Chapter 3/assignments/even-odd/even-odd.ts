
function check(){
    var num: HTMLInputElement=<HTMLInputElement>document.getElementById("num");
    var num_value = Number(num.value);
    var display: HTMLParagraphElement=<HTMLParagraphElement>document.getElementById("display");
    //console.log(num_value);
    if(num_value%2 == 0)  //condition for even number
    {
        //if even execute these statements
        display.innerHTML = '<h3>The number '+ num_value + ' is <b><u>Even Number</u></b></h3>';
    }
    else if(isNaN(num_value))
    {
        //if number is not a numeric value execute these statements
        display.innerHTML = '<h3>The number is not a valid numeric value</h3>';
    }
    else{
        //if odd execute these statements
        display.innerHTML = '<h3>The number '+ num_value + ' is <b><u>Odd Number</u></b></h3>';
    }
}