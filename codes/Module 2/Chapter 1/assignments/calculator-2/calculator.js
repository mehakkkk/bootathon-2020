//declaring variables
var num1_val;
var num2_val;
var ans;
//function for addition
function add() {
    let num1 = document.getElementById("num1");
    let num2 = document.getElementById("num2");
    var result = document.getElementById("result");
    console.log(num1.value);
    num1_val = Number(num1.value);
    num2_val = Number(num2.value);
    ans = num1_val + num2_val;
    result.value = ans.toString();
}
//function for subtraction 
function sub() {
    let num1 = document.getElementById("num1");
    let num2 = document.getElementById("num2");
    var result = document.getElementById("result");
    num1_val = Number(num1.value);
    num2_val = Number(num2.value);
    ans = num1_val - num2_val;
    result.value = ans.toString();
}
//function gor multiplication
function mul() {
    let num1 = document.getElementById("num1");
    let num2 = document.getElementById("num2");
    var result = document.getElementById("result");
    num1_val = Number(num1.value);
    num2_val = Number(num2.value);
    ans = num1_val * num2_val;
    result.value = ans.toString();
}
//function for division
function div() {
    let num1 = document.getElementById("num1");
    let num2 = document.getElementById("num2");
    var result = document.getElementById("result");
    num1_val = Number(num1.value);
    num2_val = Number(num2.value);
    ans = num1_val / num2_val;
    result.value = ans.toString();
}
//function for sin
function sin() {
    let num1 = document.getElementById("num1"); //for degree
    let num2 = document.getElementById("num2"); //for radian
    var result = document.getElementById("result");
    num1_val = Number(num1.value);
    num2_val = Number(num2.value);
    if (num1_val != 0) {
        console.log(num2_val);
        //convert in degree
        //textbox1 takes angle in degree
        ans = Number(Math.sin((Math.PI / 180) * num1_val));
    }
    else if (num2_val != 0) {
        ans = Number(Math.sin(num2_val));
    }
    result.value = ans.toString();
}
//function for cos
function cos() {
    let num1 = document.getElementById("num1"); //for degree
    let num2 = document.getElementById("num2"); //for radian
    var result = document.getElementById("result");
    num1_val = Number(num1.value);
    num2_val = Number(num2.value);
    if (num1_val != 0) {
        console.log(num2_val);
        //convert in degree
        //textbox1 takes angle in degree
        ans = Number(Math.cos((Math.PI / 180) * num1_val));
    }
    else if (num2_val != 0) {
        ans = Number(Math.cos(num2_val));
    }
    result.value = ans.toString();
}
//function for tan
function tan() {
    let num1 = document.getElementById("num1"); //for degree
    let num2 = document.getElementById("num2"); //for radian
    var result = document.getElementById("result");
    num1_val = Number(num1.value);
    num2_val = Number(num2.value);
    if (num1_val != 0) {
        console.log(num2_val);
        //convert in degree
        //textbox1 takes angle in degree
        ans = Number(Math.tan((Math.PI / 180) * num1_val));
    }
    else if (num2_val != 0) {
        ans = Number(Math.tan(num2_val));
    }
    result.value = ans.toString();
}
//function for sqaure root
function SQRT() {
    let num1 = document.getElementById("num1"); //for degree
    let num2 = document.getElementById("num2"); //for radian
    var result = document.getElementById("result");
    num1_val = Number(num1.value);
    num2_val = Number(num2.value);
    if (num1_val != 0) {
        //console.log(num2_val);
        //convert in degree
        //textbox1 takes angle in degree
        ans = Number(Math.sqrt(num1_val));
    }
    else if (num2_val != 0) {
        ans = Number(Math.sqrt(num2_val));
    }
    result.value = ans.toString();
}
//function for power
function power() {
    let num1 = document.getElementById("num1"); //for degree
    let num2 = document.getElementById("num2"); //for radian
    var result = document.getElementById("result");
    num1_val = Number(num1.value);
    num2_val = Number(num2.value);
    if (num1_val != 0) {
        //console.log(num2_val);
        //convert in degree
        //textbox1 takes angle in degree
        ans = Number(Math.pow(num1_val, num2_val));
    }
    result.value = ans.toString();
}
//# sourceMappingURL=calculator.js.map