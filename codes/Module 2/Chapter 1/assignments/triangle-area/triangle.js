function calculate() {
    var x1 = document.getElementById("x1");
    var y1 = document.getElementById("y1");
    var x2 = document.getElementById("x2");
    var y2 = document.getElementById("y2");
    var x3 = document.getElementById("x3");
    var y3 = document.getElementById("y3");
    var x1_val = Number(x1.value);
    var x2_val = Number(x2.value);
    var y1_val = Number(y1.value);
    var y2_val = Number(y2.value);
    var x3_val = Number(x3.value);
    var y3_val = Number(y3.value);
    var a = (Math.sqrt(Math.pow(x2_val - x1_val, 2) + Math.pow(y2_val - y1_val, 2)));
    var b = (Math.sqrt(Math.pow(x1_val - x3_val, 2) + Math.pow(y1_val - y3_val, 2)));
    var c = (Math.sqrt(Math.pow(x3_val - x2_val, 2) + Math.pow(y3_val - y2_val, 2)));
    console.log(a + " " + b + " " + c);
    var s = Number((a + b + c) / 2);
    var area = Number(Math.sqrt(s * ((s - a) * (s - b) * (s - c))));
    //area = area.toPrecision(4);
    var display = document.getElementById("display");
    display.innerHTML = '<h3>Area of triangle is: ' + area.toPrecision(4);
    +' </h3>';
}
//# sourceMappingURL=triangle.js.map