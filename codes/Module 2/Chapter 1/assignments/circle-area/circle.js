function submit() {
    var radius = document.getElementById("radius");
    var radius_val = Number(radius.value);
    var display = document.getElementById("display");
    var area = Math.PI * Math.pow(radius_val, 2);
    area = Number(area.toPrecision(4));
    display.innerHTML = '<h3>area of circle with radius <b> ' + radius_val + '</b> is: <u>' + area + '</u></h3>';
}
//# sourceMappingURL=circle.js.map