function submit()
{
    var radius: HTMLInputElement=<HTMLInputElement>document.getElementById("radius");
    var radius_val= Number(radius.value);
    var display: HTMLParagraphElement=<HTMLParagraphElement>document.getElementById("display");
    var area = Math.PI*Math.pow(radius_val,2);//calculate area of circle 
    area = Number(area.toPrecision(4));//setting decimal precision
    display.innerHTML = '<h3>area of circle with radius <b> '+radius_val+'</b> is: <u>'+ area+'</u></h3>';
}