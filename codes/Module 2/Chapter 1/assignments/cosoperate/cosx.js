function solve() {
    var x = document.getElementById("number");
    var x_value = Number(x.value); //get value of x
    var ans = Number(x_value - Math.cos(x_value)); //solving for x
    ans = Number(ans.toPrecision(4)); //decimal digits precision
    var display = document.getElementById("display");
    display.innerHTML = "X = " + x_value + " after solving value is: " + ans; //displaying value
}
//# sourceMappingURL=cosx.js.map