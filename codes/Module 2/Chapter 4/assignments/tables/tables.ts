function create():void{
    var input:HTMLInputElement = <HTMLInputElement>document.getElementById("input");
    var table:HTMLTableElement = <HTMLTableElement>document.getElementById("table");
    var count:number = 1;
    var num:number = +input.value;
    var ans:number = 1;
    while( table.rows.length > 1)   {
        table.deleteRow(1);
    }

    
    for(count=1; count<=10 ; count++)  {
        //for printing count from 1 to 10
        var row:HTMLTableRowElement = table.insertRow();

        var cell:HTMLTableDataCellElement = row.insertCell();

        var text:HTMLInputElement = document.createElement("input");
        text.type = "text";
        text.id = "t"+count;
        text.style.textAlign = "center";
        text.style.background = "lime";
        text.style.fontSize ="20px";

        text.value = count.toString();
        cell.appendChild(text);
        //for printing *
        var cell:HTMLTableDataCellElement = row.insertCell();

        var text:HTMLInputElement = document.createElement("input");
        text.type = "text";
        text.id = "tt"+count;
        var c="*";
        text.value = c.toString();
        text.style.textAlign = "center";
        text.style.background = "orange";
        table.style.margin= "40px";
        cell.appendChild(text);
        text.style.fontSize ="20px";

        //for printing input value
        var cell:HTMLTableDataCellElement = row.insertCell();

        var text:HTMLInputElement = document.createElement("input");
        text.type = "text";
        text.id = "tt1"+count;
        text.value = num.toString();
        text.style.textAlign = "center";
        text.style.background = "lime";
        table.style.margin= "40px";
        cell.style.width = "0.5px";
        cell.appendChild(text);
        text.style.fontSize ="20px";

        //for printing =
        var cell:HTMLTableDataCellElement = row.insertCell();

        var text:HTMLInputElement = document.createElement("input");
        text.type = "text";
        text.id = "tt2"+count;
        var c="=";
        text.value = c.toString();
        text.style.textAlign = "center";
        text.style.background = "orange";
        table.style.margin= "40px";
        cell.appendChild(text);
        text.style.fontSize ="20px";

        //now for printing 1*num value
        var cell:HTMLTableDataCellElement = row.insertCell();

        var text:HTMLInputElement = document.createElement("input");
        text.type = "text";
        text.id = "tt3"+count;
        ans = num*count;
        text.value = ans.toString();
        text.style.textAlign = "center";
        text.style.background = "lime";
        table.style.margin= "40px";
        cell.appendChild(text);
        text.style.fontSize ="20px";

    }
}
